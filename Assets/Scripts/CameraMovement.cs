﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	float speed = 0.01f;
	private Vector3 lastPosition;
	private GameObject map;
	private Vector3 cameraOriginalPosition;

	public float viewportWidth;
	public float viewportHeight;


	// Use this for initialization

	void Awake() {
	
		map = GameObject.FindGameObjectWithTag("BuildingLayer");
		SpriteRenderer renderer = map.GetComponent<SpriteRenderer> ();
		Vector3 size = renderer.bounds.size;

		viewportHeight = Camera.main.orthographicSize * 2;
		viewportWidth = viewportHeight * Camera.main.aspect;
	}
	
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (0)) {
			lastPosition = Input.mousePosition;
		}

		if (Input.GetMouseButtonUp (0)) {
			Vector3 screenToViewport = Camera.main.ScreenToViewportPoint (Input.mousePosition);
			Vector3 screenToWorld = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		}

		if (Input.GetMouseButton (0)) {
			Vector3 delta = Input.mousePosition - lastPosition;
			transform.Translate (-delta.x * speed, -delta.y * speed, 0f);
			lastPosition = Input.mousePosition;
		}

		//ZOOM

		const int orthographicSizeMin = 1; const int orthographicSizeMax = 6;
		
		if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
		{
			Camera.main.orthographicSize++;
			viewportHeight = Camera.main.orthographicSize * 2;
			viewportWidth = viewportHeight * Camera.main.aspect;
		}
		if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
		{
			Camera.main.orthographicSize--;
			viewportHeight = Camera.main.orthographicSize * 2;
			viewportWidth = viewportHeight * Camera.main.aspect;
		}
		
		Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, orthographicSizeMin, orthographicSizeMax );
	}

	public void startCameraEffect()
	{
		cameraOriginalPosition = transform.position;
		InvokeRepeating ("shakeCamera", 0, 0.02f);
		Invoke ("stopShaking", 0.06f);
	}
	const float shakeValue = 0.1f;
	void shakeCamera()
	{
		Vector3 cameraPosition = transform.position;

		//float shakeOffsetX = Random.value * shakeValue * 2 - shakeValue;
		float shakeOffsetX = Mathf.Sin(Time.time);
		
		float shakeOffsetY = Random.value * shakeValue * 2 - shakeValue;

		//cameraPosition.x += shakeOffsetX;
		cameraPosition.y += shakeOffsetY;

		transform.position = cameraPosition;
	}

	void stopShaking() {
		CancelInvoke("shakeCamera");
		transform.position = cameraOriginalPosition;
	}
}
