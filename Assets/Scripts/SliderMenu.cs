﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SliderMenu : MonoBehaviour {
   
	enum SlideTo 
	{
		Left = 0,
		Right = 1,
	}

	public GameObject draggerObject;
	public Button leftButton;
	public Button rightButton;
	public List<Button>buttons;

	DragOn dragger;
	XMLParser parser;
	List<MenuItemModel> menuItemList;
	List<Sprite> spriteList;

	int sliderOffset;
	int sliderEnd;

	// Use this for initialization
	void Start () {
		dragger 		= draggerObject.GetComponent<DragOn> ();
		parser 			= XMLParser.Instance;
		menuItemList    = parser.getMenuItemList ();
		spriteList 	    = new List<Sprite> ();

		createMenuItems ();
		configureSlider (Mathf.Abs(spriteList.Count-buttons.Count));
		initializeButtons ();
	}

	void createMenuItems()
	{
		foreach (MenuItemModel menuItemModel in menuItemList) {
			string imageName = menuItemModel.ImageName;
			Texture2D texture = (Texture2D)Resources.Load("Menu/" + imageName);
			Sprite sprite = Sprite.Create(texture, new Rect(0, 0, 85, 60), new Vector2(0f, 0f));
			spriteList.Add(sprite);
		}
	}

	void configureSlider(int length)
	{
		sliderOffset = 0;
		sliderEnd = length; 
	}

	void initializeButtons()
	{
		for (int i = 0; i < buttons.Count; i++) {
			Sprite sprite = spriteList[i];
			Button button = buttons[i];
			button.image.sprite = sprite;

			MenuItemModel menuItem = menuItemList[i];
			Text text = button.gameObject.GetComponentInChildren<Text>();
			text.text = menuItem.Title;
		}
	}

	void slide(SlideTo dir)
	{
		if (dir == SlideTo.Right) {

			sliderOffset++;
		}else if (dir == SlideTo.Left) {
			sliderOffset--;
		}

		for (int i = 0; i < buttons.Count; i++) {
			Sprite sprite = spriteList[i + sliderOffset];
			Button button = buttons[i];
			button.image.sprite = sprite;

			MenuItemModel menuItem = menuItemList[i + sliderOffset];
			Text text = button.gameObject.GetComponentInChildren<Text>();
			text.text = menuItem.Title;
		}
	}
	// Button listeners

	public void OnItemSelected(Button button)
	{
		int indexOfButton = buttons.FindIndex ((Button x) => x == button);
		MenuItemModel menuItemModel = menuItemList [indexOfButton + sliderOffset];
		dragger.prepareSelectedPrefab (menuItemModel.PrefabName);
	}

	public void OnLeftButtonPressed()
	{
		if (sliderOffset >= 1) {
			slide(SlideTo.Left);
		}
	}

	public void OnRightButtonPressed()
	{
		if (sliderOffset < sliderEnd) {
			slide(SlideTo.Right);
		}
	}

	public void OnCanvasOver()
	{
		dragger.setDragOnActiveState (false);
	}

	public void OnCanvasExit()
	{
		dragger.setDragOnActiveState (true);
	}
}
