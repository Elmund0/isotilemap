﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Popup : MonoBehaviour {

    SpriteRenderer renderer;
	Text text;
	// Use this for initialization
	void Start () {
        renderer = GetComponent<SpriteRenderer>();
        renderer.color = new Color(1f, 1f, 1f, 0f);
		text = GetComponentInChildren<Text>();
		text.color = new Color(1f, 1f, 1f, 0f);
	}

	private IEnumerator fadeIn() {
		for (float f = 0; f < 1f; f += 0.1f) {
			Color color = this.renderer.color;
			color.a = f;
			this.renderer.color = color;
			text.color = color;
			yield return null;
		}

		Invoke ("showTimeEnded", 2f);
	}

	private IEnumerator fadeOut() {
		for (float f = 1f; f > 0; f -= 0.1f) {
			Color color = this.renderer.color;
			color.a = f;
			this.renderer.color = color;
			text.color = color;
			yield return null;
		}

		Color c = this.renderer.color;
		c.a = 0f;
		this.renderer.color = c;
		text.color = c;
	}

    public void show(Transform transform, string text, Color textColor)
    {
		StopAllCoroutines ();
		CancelInvoke ("showTimeEnded");
        
		this.text.text = text;
		this.text.material.color = textColor;
		float newYPosition = transform.position.y + transform.GetComponent<SpriteRenderer>().sprite.bounds.size.y - 0.18f;
        Vector3 newPos = new Vector3(transform.position.x, newYPosition, transform.position.z);
        this.transform.position = newPos;
        gameObject.SetActive(true);

		StartCoroutine ("fadeIn");
    }

	public void showTimeEnded()
	{
		StartCoroutine("fadeOut");
	}

    public void hide()
    {
        gameObject.SetActive(false);
    }

}
