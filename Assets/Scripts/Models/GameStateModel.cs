﻿using UnityEngine;
using System.Collections;

public class GameStateModel {

    public GameStateModel()
    {
;
    }

    public GameStateModel(string prefabName, int mapX, int mapY)
    {
        this.PrefabName = prefabName;
        this.MapX = mapX;
        this.MapY = mapY;
    }

    public string PrefabName { get; set; }
    public int MapX
    {
        get;
        set;
    }
    public int MapY
    {
        get;
        set;
    }
}
