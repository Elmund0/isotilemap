﻿using UnityEngine;
using System.Collections;

public class MenuItemModel {

	public string ImageName{ get; set;}
	public string PrefabName {
		get;
		set;
	}
	public string Title {
		get;
		set;
	}
}
