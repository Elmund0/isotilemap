﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Reflection;
using System;

public class XMLParser {

	XmlDocument doc;

	private static XMLParser instance;
	public static XMLParser Instance {
		get{ 
			if(instance==null) 
				instance = new XMLParser(); 

			return instance; 
		}
	}

	public XMLParser ()
	{
		doc = new XmlDocument ();
	}

	public List<MenuItemModel> getMenuItemList()
	{
		doc.Load("./Assets/Resources/XML/menu_items.xml");
		XmlElement root = doc.DocumentElement;
		XmlNodeList list = root.GetElementsByTagName("menuItem");
		List<MenuItemModel> itemList = new List<MenuItemModel>();
		foreach (XmlElement item in list) {
			MenuItemModel model = new MenuItemModel();
			foreach (XmlElement itemProperty in item) {
				string key = itemProperty.Name;
				PropertyInfo pinfo = model.GetType().GetProperty(key);
				pinfo.SetValue(model, itemProperty.InnerText, null);
			}
			
			itemList.Add(model);
		}

		return itemList;
	}

    public List<GameStateModel> getSaveFileItemList()
    {
        doc.Load("./Assets/Resources/XML/save_file.xml");
        XmlElement root = doc.DocumentElement;
        XmlNodeList list = root.GetElementsByTagName("item");
        List<GameStateModel> itemList = new List<GameStateModel>();
        foreach (XmlElement item in list)
        {
            GameStateModel model = new GameStateModel();
            foreach (XmlElement itemProperty in item)
            {
                string key = itemProperty.Name;
                PropertyInfo pinfo = model.GetType().GetProperty(key);
                pinfo.SetValue(model, Convert.ChangeType(itemProperty.InnerText, pinfo.PropertyType), null);
            }

            itemList.Add(model);
        }

        return itemList;
    }

    public void saveGameStateItem(GameStateModel model)
    {
        doc.Load("./Assets/Resources/XML/save_file.xml");
        XmlElement root = doc.DocumentElement;

        XmlElement itemElement = doc.CreateElement("item");
        XmlElement prefabElement = doc.CreateElement("PrefabName");
        XmlElement mapX = doc.CreateElement("MapX");
        XmlElement mapY = doc.CreateElement("MapY");

        prefabElement.InnerText = model.PrefabName;
        mapX.InnerText = model.MapX.ToString();
        mapY.InnerText = model.MapY.ToString();

        itemElement.AppendChild(prefabElement);
        itemElement.AppendChild(mapX);
        itemElement.AppendChild(mapY);

        root.AppendChild(itemElement);
        doc.Save("./Assets/Resources/XML/save_file.xml");
    }

}
