﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

class IsoRendererComp: IComparer<SpriteRenderer>
{

    public int Compare(SpriteRenderer x, SpriteRenderer y)
    {
        IsoSpriteRenderer isoX = x.GetComponent<IsoSpriteRenderer>();
        IsoSpriteRenderer isoY = y.GetComponent<IsoSpriteRenderer>();
        if (isoX.isoDepth > isoY.isoDepth)
        {
            return 1;
        }
        else if (isoX.isoDepth < isoY.isoDepth)
        {
            return -1;
        }

        return 0;
    }

}
