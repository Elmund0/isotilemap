﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Utils {

    const float TileWidthHalf = 0.64f;
    const float TileHeightHalf = 0.32f;

    public static Vector3 convertMapToIso(Vector3 map)
    {
        float screenX = (map.x - map.y) * TileWidthHalf;
        float screenY = -(map.x + map.y) * TileHeightHalf; // Unity y axis is reverse
        return new Vector3(screenX, screenY, 0f);
    }

    public static Vector3 convertIsoToMap(Vector3 iso)
    {
        iso.y = -iso.y;
		iso.y -= TileHeightHalf; // Because of anchor point center, offset half of height top
        float mapX = Mathf.CeilToInt((iso.x / TileWidthHalf + iso.y / TileHeightHalf) / 2);
        float mapY = Mathf.CeilToInt((iso.y / TileHeightHalf - (iso.x / TileWidthHalf)) / 2);
        return new Vector3(mapX, mapY, 0f);
    }

	public static void isometricSort(IList renderers) {

		determineDependencies (renderers);
        sortByIsoDepth(renderers);
	}

	public static void sortByIsoDepth(IList renderers)
	{
        SpriteRenderer[] array = renderers as SpriteRenderer[];
        List<SpriteRenderer> list = new List<SpriteRenderer>();
        for (int i = 0; i < array.Length; i++)
        {
            list.Add(array[i]);
        }

        list.Sort(new IsoRendererComp());
		//TODO:(Baris) After sorting operations, sort the renderers array according to sort value of list
		for (int i = 0; i < list.Count; i++) {
			renderers[i] = list[i];
		}

		for (int i = 0; i < list.Count; i++)
        {
			SpriteRenderer rend = list[i] as SpriteRenderer;
            rend.sortingOrder = i;
        }
	}

	private static void swapSortingOrders(SpriteRenderer first, SpriteRenderer second) {
		int sortingIndex = first.sortingOrder;
		first.sortingOrder = second.sortingOrder;
		second.sortingOrder = sortingIndex;
		
		first.transform.position = new Vector3(first.transform.position.x, 
		                                       first.transform.position.y,
		                                       -first.sortingOrder/10f);
		second.transform.position = new Vector3(second.transform.position.x, 
		                                        second.transform.position.y,
		                                        -second.sortingOrder/10f);
	}

	public static void determineDependencies(IList sprites)
	{
		// Set dependencies
		IsoSpriteRenderer a = null;
		IsoSpriteRenderer b = null;
		int behindIndex;
		for (int i = 0; i < sprites.Count; i++) {
			SpriteRenderer rend = sprites[i] as SpriteRenderer;
			a = rend.GetComponent<IsoSpriteRenderer>();
			behindIndex = 0;

			for (int j = 0; j < sprites.Count; j++) {
				if (i != j) {
					SpriteRenderer rendB = sprites[j] as SpriteRenderer;
					b = rendB.GetComponent<IsoSpriteRenderer>();
					// b object is behind of a object

					double bMinY = System.Math.Round(b.minY, 2);
					double aMinY = System.Math.Round(a.minY, 2); 

					if (bMinY > aMinY) { 
                        a.isoSpriteBehind.Insert(behindIndex++, b);
					}

					else if (bMinY == aMinY) {
						Building aBuilding = a.GetComponent<Building>();
						Building bBuilding = b.GetComponent<Building>();

						if (aBuilding.TileSize < bBuilding.TileSize) {
							a.isoSpriteBehind.Insert(behindIndex++, b);
						}
					}

				}
			}
		
			a.isoVisitedFlag = 0;
		}

		// Make topological sort operation
		topologicalSort (sprites);
	}

	private static int _sortDepth;
	private static void topologicalSort(IList objects)
	{
		_sortDepth = 0;
		for (int i = 0; i < objects.Count; i++) {

			SpriteRenderer renderer = objects[i] as SpriteRenderer;
			IsoSpriteRenderer isoRenderer = renderer.GetComponent<IsoSpriteRenderer>();
			visitNode(isoRenderer);
		}
	}

	private static void visitNode(IsoSpriteRenderer n)
	{
		if (n.isoVisitedFlag == 0) {
			n.isoVisitedFlag = 1;

		    int spritesBehindLength = n.isoSpriteBehind.Count;
			for (int i = 0; i < spritesBehindLength; i++) {
				if (n.isoSpriteBehind[i] == null) {
					break;
				}else {
					visitNode(n.isoSpriteBehind[i]);
					n.isoSpriteBehind[i] = null;
				}
			}

			n.isoDepth = _sortDepth++;

		}
	}
}
