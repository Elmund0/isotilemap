﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IsoSpriteRenderer : MonoBehaviour {

	// Property References
	private SpriteRenderer renderer;

	public float isoX;
	public float isoY;

	// AABB in iso world space
	public float minX;
	public float maxX;
	public float minY;
	public float maxY;

	// Internal variables for sorting in the renderer.
	public int isoDepth;
	internal List<IsoSpriteRenderer> isoSpriteBehind;
	internal int isoVisitedFlag;

	void Awake() {
        isoSpriteBehind = new List<IsoSpriteRenderer>();
		renderer = transform.GetComponent<SpriteRenderer>();
		updateIsoData (transform, this.renderer.bounds);
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void updateIsoData(Transform transform, Bounds bounds)
	{
		Vector3 min = bounds.min;
		Vector3 max = bounds.max;

		minX = min.x;
		maxX = max.x;
		minY = min.y;
		maxY = max.y;
	}

	void calculate(Transform transform)
	{

	}
}
