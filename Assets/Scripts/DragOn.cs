﻿using UnityEngine;
using System.Collections;


public class DragOn : MonoBehaviour {

	const int DEFAULT_SPAWN_ORDER = 0;
	const int MOUSE_RIGHT_BUTTON = 1;
	const int MOUSE_LEFT_BUTTON = 0;

	TileMapManager tileMapManager;
	GameObject selectedBuildingObject;
	GameObject isoMap;
	Vector3 buildingDraggingPin;
	string selectedPrefabName = "";
	bool isActive = true;
	bool isBuildCancelled = true;
	bool isTileAvailable = true;
	int sortingOrderIndex = 1;

	Color RED_COLOR   = new Color (1f, 0.2f, 0.2f, 0.5f);
	Color GREEN_COLOR = new Color (0.2f, 1f, 0.2f, 0.5f);

	// Use this for initialization
	void Start () {
		isoMap = GameObject.FindGameObjectWithTag ("Map");
		tileMapManager = GameObject.FindGameObjectWithTag ("TileMapManager").GetComponent<TileMapManager> ();
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 screenPosition 	= Input.mousePosition;
        Vector3 worldPosition 	= Camera.main.ScreenToWorldPoint(screenPosition);
        Vector3 map 			= Utils.convertIsoToMap(worldPosition);
		Vector3 snappedCoord 	= Utils.convertMapToIso(map);

		if (selectedBuildingObject) {
			SpriteRenderer renderer = selectedBuildingObject.GetComponent<SpriteRenderer> ();
			Building building = selectedBuildingObject.GetComponent<Building>();
			if (!tileMapManager.isTilesAvailable (map, building.TileSize)) {
				renderer.color = RED_COLOR;
				isTileAvailable = false;
			} else {
				renderer.color = GREEN_COLOR;
				isTileAvailable = true;
			}
		}

		if (Input.GetMouseButtonUp(MOUSE_RIGHT_BUTTON)) {
			cancelBuilding();
		}

		if (!isBuildCancelled && selectedBuildingObject) {
			selectedBuildingObject.transform.position = snappedCoord;
		}

		if (Input.GetMouseButtonDown(MOUSE_LEFT_BUTTON)) {
			buildingDraggingPin = worldPosition;
		}

		if (Input.GetMouseButtonUp(MOUSE_LEFT_BUTTON))
        {
			if (!isBuildCancelled && isTileAvailable && isActive && selectedBuildingObject ) {
				
				GameObject tilePrefab = (GameObject)Resources.Load("Prefabs/Items/" + selectedPrefabName);
				Vector3 spawnPoint = selectedBuildingObject.transform.position;
				GameObject newBuilding = (GameObject)Instantiate(tilePrefab, spawnPoint, Quaternion.identity);
                SpriteRenderer renderer = newBuilding.GetComponent<SpriteRenderer>();
				Building building = newBuilding.GetComponent<Building>();

                tileMapManager.setTilesInRuntime(newBuilding, map, building.TileSize, selectedPrefabName);

				renderer.sortingLayerID = LayerMask.NameToLayer("Buildings");
				renderer.sortingLayerName = "Buildings";
				renderer.sortingOrder = sortingOrderIndex;
				renderer.gameObject.layer = LayerMask.NameToLayer("Buildings");
				sortingOrderIndex++;
                newBuilding.transform.parent = isoMap.transform;

				SpriteRenderer tileRenderer = selectedBuildingObject.GetComponent<SpriteRenderer>();
				tileRenderer.sortingOrder = sortingOrderIndex;
				newBuilding.transform.position = new Vector3(newBuilding.transform.position.x, 
				                                             newBuilding.transform.position.y ,
				                                             -sortingOrderIndex/10f);
				SpriteRenderer[] renderers = isoMap.GetComponentsInChildren<SpriteRenderer> ();

				Utils.isometricSort(renderers);

				CameraMovement cam = Camera.main.GetComponent<Camera>().GetComponent<CameraMovement>();
				cam.startCameraEffect();
                //SoundManager.instance.playBuildEfx();
			}

			isBuildCancelled = false;
        }

		if (Input.GetMouseButton(MOUSE_LEFT_BUTTON)) {
			if (buildingDraggingPin != worldPosition) {
				isBuildCancelled = true;
			}
		}
	}

	public void setDragOnActiveState(bool state)
	{
		isActive = state;
		if (selectedBuildingObject)
			selectedBuildingObject.SetActive (state);
	}

	private void cancelBuilding()
	{
		Destroy (selectedBuildingObject);
		selectedPrefabName = "";
		selectedBuildingObject = null;
        tileMapManager.setTileCollidersEnabled(true);
	}

	public void prepareSelectedPrefab (string prefabName)
	{
        tileMapManager.setTileCollidersEnabled(false);
		Destroy (selectedBuildingObject);
		selectedPrefabName = prefabName;
		GameObject tilePrefab = (GameObject)Resources.Load("Prefabs/Items/" + selectedPrefabName);
		Vector3 spawnPoint = transform.position;
		spawnPoint.z = DEFAULT_SPAWN_ORDER;
		selectedBuildingObject = (GameObject)Instantiate (tilePrefab, spawnPoint, Quaternion.identity);
        SpriteRenderer renderer = selectedBuildingObject.GetComponent<SpriteRenderer>();

		renderer.sortingLayerID = LayerMask.NameToLayer ("Decoration");
		renderer.sortingLayerName = "Decoration";
		renderer.gameObject.layer = LayerMask.NameToLayer("Decoration");
		renderer.color = GREEN_COLOR;
		setDragOnActiveState (false);
	}
}
