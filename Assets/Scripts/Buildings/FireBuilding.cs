﻿using UnityEngine;
using System.Collections;

public class FireBuilding : Building {

	protected override void initialize()
	{
		textColor = new Color (213f/255f, 61f/255f, 23f/255f);
	}
	
	protected override void showPopup()
	{
		popup.show(transform, BuildingText, textColor);
	}
}
