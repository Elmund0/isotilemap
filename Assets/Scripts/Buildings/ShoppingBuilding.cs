﻿using UnityEngine;
using System.Collections;

public class ShoppingBuilding : Building {

	protected override void initialize()
	{
		textColor = new Color (210f/255f, 117f/255f, 211f/255f);
	}
	
	protected override void showPopup()
	{
		popup.show(transform, BuildingText, textColor);
	}
}
