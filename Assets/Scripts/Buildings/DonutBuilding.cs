﻿using UnityEngine;
using System.Collections;

public class DonutBuilding : Building {

	protected override void initialize()
	{
		textColor = new Color (233f/255f, 183f/255f, 75f/255f);
	}

	protected override void showPopup()
	{
		popup.show(transform, BuildingText, textColor);
	}
}
