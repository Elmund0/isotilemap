﻿using UnityEngine;
using System.Collections;

public class Building : MonoBehaviour {

	public int TileSize;
	public string BuildingText;
	protected Color textColor = new Color (1f, 1f, 1f, 1f);
    protected Popup popup;

	// Use this for initialization

	void Awake() {
		GameObject popupObject = GameObject.FindGameObjectWithTag("Popup");
		popup = popupObject.GetComponent<Popup>();
	}

	void Start () {
		initialize ();
	}

	// Override this method
	protected virtual void initialize()
	{
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnMouseDown()
    {
        showPopup();
    }

    protected virtual void showPopup()
    {
		popup.show(transform, BuildingText, textColor);
    }
}
