﻿using UnityEngine;
using System.Collections;

public class PoliceBuilding : Building {

	protected override void initialize()
	{
		textColor = new Color (227f/255f, 227f/255f, 227f/255f);
	}
	
	protected override void showPopup()
	{
		popup.show(transform, BuildingText, textColor);
	}
}
