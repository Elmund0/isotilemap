﻿using UnityEngine;
using System.Collections;

public class UniqueBuilding0 : Building {
	
	protected override void initialize()
	{
		textColor = new Color (222f/255f, 189f/255f, 126f/255f);
	}

    protected override void showPopup()
    {
		popup.show(transform, BuildingText, textColor);
    }
}
