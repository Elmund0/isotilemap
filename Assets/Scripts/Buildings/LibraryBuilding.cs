﻿using UnityEngine;
using System.Collections;

public class LibraryBuilding : Building {

	protected override void initialize()
	{
		textColor = new Color (238f/255f, 221f/255f, 193f/255f);
	}
	
	protected override void showPopup()
	{
		popup.show(transform, BuildingText, textColor);
	}
}
