﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileMapManager : MonoBehaviour {

	// Prefab reference
	public Transform greenTile;
	public Transform redTile;

    GameObject isoMap;
	int[,] map = new int[20, 20];
    GameObject[,] buildings = new GameObject[20, 20];
	const float tileWidth = 0.128f;
	const float tileHeight = 0.64f;
	const int MAP_TILE_SIZE = 20;

    void Awake()
    {
        isoMap = GameObject.FindGameObjectWithTag("Map");
    }

	// Use this for initialization
	void Start () {
		for (int i = 0; i < MAP_TILE_SIZE; i++) {
			for (int j = 0; j < MAP_TILE_SIZE; j++) {
				map[i, j] = 0;
			}
		}

        loadSaveFile();
	}

    private void loadSaveFile()
    {
        List<GameStateModel> buildingsToInstantiate = XMLParser.Instance.getSaveFileItemList();
        int sortingOrderIndex = 1;
        foreach (GameStateModel model in buildingsToInstantiate)
        {
            instantiateBuilding(model, sortingOrderIndex);
            sortingOrderIndex++;
        }

        setTileCollidersEnabled(true);
    }

    private void instantiateBuilding(GameStateModel model, int sortingOrder)
    {
        Vector3 position = Utils.convertMapToIso(new Vector3((float)model.MapX, (float)model.MapY, 0));
        GameObject tilePrefab = (GameObject)Resources.Load("Prefabs/Items/" + model.PrefabName);
        GameObject newBuilding = (GameObject)Instantiate(tilePrefab, position, Quaternion.identity);
        SpriteRenderer renderer = newBuilding.GetComponent<SpriteRenderer>();
        Building building = newBuilding.GetComponent<Building>();

        renderer.sortingLayerID = LayerMask.NameToLayer("Buildings");
        renderer.sortingLayerName = "Buildings";
        renderer.sortingOrder = sortingOrder;
        renderer.gameObject.layer = LayerMask.NameToLayer("Buildings");

        newBuilding.transform.parent = isoMap.transform;

        SpriteRenderer tileRenderer = newBuilding.GetComponent<SpriteRenderer>();
        tileRenderer.sortingOrder = sortingOrder;
		//newBuilding.transform.position.z = sortingOrderIndex/10f; TODO:(Baris) There is no sortingIndex data.
        SpriteRenderer[] renderers = isoMap.GetComponentsInChildren<SpriteRenderer>();

        setTilesFromFile(newBuilding, new Vector3((float)model.MapX, (float)model.MapY, 0), building.TileSize);
        Utils.isometricSort(renderers);
    }

    private void setTiles(GameObject obj, Vector3 pos, int tileSize)
    {
        int x = (int)pos.x;
        int y = (int)pos.y;
        buildings[x, y] = obj;

        for (int i = 0; i < tileSize; i++)
        {
            for (int j = 0; j < tileSize; j++)
            {
                map[y - j, x - i] = 1;
            }
        }
    }

    private void setTilesFromFile(GameObject obj, Vector3 pos, int tileSize)
    {
        setTiles(obj, pos, tileSize);
    }

    public void setTilesInRuntime(GameObject obj, Vector3 pos, int tileSize, string prefabName)
	{
        setTiles(obj, pos, tileSize);
        GameStateModel stateModel = new GameStateModel(prefabName, (int)pos.x, (int)pos.y);
        XMLParser.Instance.saveGameStateItem(stateModel);
	}

	public bool isTilesAvailable(Vector3 pos, int tileSize)
	{
		int x = (int)pos.x;
		int y = (int)pos.y;

		for (int i = 0; i < tileSize; i++) {
			for (int j = 0; j < tileSize; j++) {

				if (x-i<0 || y-j<0 || x-i>MAP_TILE_SIZE-1|| y-j>MAP_TILE_SIZE-1) {
					//Debug.Log("Tile not available!: X=" + (x-i).ToString() + " Y=" + (y-j));
					return false;
				}

				if(map[y - j, x - i] == 1) {
					//Debug.Log("Tile not available!: X=" + (x-i).ToString() + " Y=" + (y-j));
					return false;

				}
			}
		}

		return true;
	}

    public void setTileCollidersEnabled(bool state)
    {
		for (int i = 0; i < MAP_TILE_SIZE; i++)
        {
			for (int j = 0; j < MAP_TILE_SIZE; j++)
			{
                GameObject building = buildings[i, j];
                if (building)
                {
                    //Debug.Log("All colliders are ignored = " + state);
                    Collider2D collider = building.GetComponent<Collider2D>();
                    collider.enabled = state;
                }
            }
        }
    }
}
